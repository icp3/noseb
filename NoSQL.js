
let body = document.getElementById("body");
let firebaseConfig = {
    apiKey: "AIzaSyD5eeibJNP0vEh_9isPd1kglzF1YGYUSoQ",
    authDomain: "nowasm-109f9.firebaseapp.com",
    databaseURL: "https://nowasm-109f9.firebaseio.com",
    projectId: "nowasm-109f9",
    storageBucket: "nowasm-109f9.appspot.com",
    messagingSenderId: "416660245033",
    appId: "1:416660245033:web:8d905107d3ad9598ab6a38",
    measurementId: "G-ZRZWFMRPTC"
};
// Initialize Firebase
let db;

let check = true;
window.onhashchange = loadSite();

function addList(doc){
    let ret = "<" + getListType() + getAttributes(doc) + ">"
    db.collection(site).where("")
}

function loadSite(){
    let page = "", head = "", foot = "";
    if(check) {
        firebase.initializeApp(firebaseConfig);
        db = firebase.firestore();
        check = false;
    }
    console.log("Load Collection");
    db.collection(site).where("type", "==", Web).get().then((snapshot) => {
        snapshot.docs.forEach(doc => {
            page += addItem(doc);
        });
        db.collection(site).where("type", '==', Header).get().then((snap) => {
            snap.docs.forEach(hea => {
                head += addItem(hea);
            });
            db.collection(site).where("type", '==', Footer).get().then((snap) => {
                snap.docs.forEach(hea => {
                    foot += addItem(hea);
                });
                body.innerHTML = head + page + foot;
            });
        });
    });

    setTimeout(() => { loadSite(); }, 500);
}

function addItem(doc){
    let ret = "";
    if(doc.data().tag === "p")
        ret += addP(doc);
    else if(doc.data().tag === "button")
        ret += addButton(doc);
    else if(doc.data().tag === "div")
        ret += addDiv(doc);
    else if(doc.data().tag === "a")
        ret += addHyperlink(doc);
    else if(doc.data().tag === "table")
        ret += addTable(doc);
    else if(doc.data().tag === "css")
        ret += addCSSItem(doc);
    return ret;
}

function addTable(doc){
    return "<table " + getAttributes(doc) + ">" + doc.data().text + "</table>";
}

function addDiv(doc) {
    return "<div" + getAttributes(doc) + ">" + doc.data().text + "</div>";
}

function addHyperlink(doc){
    return "<a" + getAttributes(doc) + ">" + getText(doc) + "</a>";
}



function addButton(doc){
    return "<button" + getAttributes(doc) + ">" +getText(doc) + "</button>";
}

function addP(doc){
    return "<p" + getPSize(doc) + getAttributes(doc) + ">" + getText(doc) + "</\p" + getPSize(doc) + ">"
}

function getText(doc){
    if(doc.data().text === undefined)
        return doc.data().tag;
    return doc.data().text;
}

function getAttributes(doc){
    let hyp = getHref(doc);
    let title = getTitle(doc);
    let css = getCSS(doc);
    let onClick = getOnClick(doc);
    let cellpadding = getCellPadding(doc);
    let cellspacing = getCellSpacing(doc);
    let width = getWidthAttr(doc);
    let ret = "";
    if(hyp !== "")
        ret += " " + hyp;
    if(title !== "")
        ret += " " + title;
    if(css !== "")
        ret += " " + css;
    if(onClick !== "")
        ret += " " + onClick;
    if(cellpadding !== "")
        ret += " " + cellpadding;
    if(cellspacing !== "")
        ret += " " + cellspacing;
    if(width !== "")
        ret += " " + width;
    return ret;
}

function getWidthAttr(doc){
    if(doc.data().width !== undefined)
        return "width=\"" + doc.data().width + "\"";
    return "";
}

function addCSSItem(doc){
    return "<style type=\"text/css\">" + doc.data().text + "</style>";
}

function getCellSpacing(doc){
    if(doc.data().cellspacing !== undefined)
        return "cellspacing=" + doc.data().cellspacing;
    return "";
}

function getCellPadding(doc){
    if(doc.data().cellpadding !== undefined)
        return "cellpadding=" + doc.data().cellpadding;
    return "";
}

function getOnClick(doc){
    if(doc.data().onClick !== undefined)
        return "onclick=\"" + doc.data().onclick + "\"";
    return "";
}

function getPSize(doc){
    if(doc.data().pSize !== undefined)
        return doc.data().pSize.toString();
    return "";
}

function getHref(doc){
    if(doc.data().hyperlink !== undefined)
        return "href=\"" + doc.data().hyperlink.toString() + "\"";
    return "";
}

function getTitle(doc){
    if(doc.data().title !== undefined)
        return "title=\"" + doc.data().title.toString() + "\"";
    return "";
}

function getCSS(doc){
    if(doc.data().getCSS !== undefined)
        return "style=\"" + doc.data().getCSS.toString() + "\"";
    return "";
}

